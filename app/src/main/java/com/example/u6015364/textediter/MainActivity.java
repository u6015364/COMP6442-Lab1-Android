package com.example.u6015364.textediter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> list = new ArrayList<String>();
    private int count = 0;
    private int max = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void previous(View v){
        if(count > 0) {
            EditText tv = (EditText) findViewById(R.id.editText);
            if(max!=count) {
                list.remove(count);
            }
            list.add(count, tv.getText().toString());
            count--;
            tv.setText(list.get(count));
        }

    }

    public void next(View v){

        EditText tv = (EditText) findViewById(R.id.editText);
        if(count != max) {
            list.remove(count);
            list.add(count, tv.getText().toString());
            count++;
            tv.setText(list.get(count));
        }
        else {
            list.add(tv.getText().toString());
            count++;
            tv.setText("");
        }
        if(max < count){
            max = count;
        }
    }

    public void delete(View v){
        if(max != count){
            list.remove(count);
            max--;
        }
        EditText tv = (EditText) findViewById(R.id.editText);
        if(count != 0) {
            count--;
        }

        tv.setText(list.get(count));
    }
}
